import os
import re
import subprocess
import time

import sublime_plugin

_DEBUG = True


class FormatPythonCommand(sublime_plugin.EventListener):
    """
    on-save this plugin will run "invoke format" for your project.

    venv: the path to your virtual environment
    invoke_folder: the folder where "tasks.py" lives

    Required project settings:

        "tommy_formats": {
                "venv": "${project_path}/venv",
                "invoke_folder": "${project_path}/backend",
        }

    Run isort & black on save
    """

    def _get_project_settings(self, view):
        if not getattr(self, '_PROJECT_SETTINGS', None):
            self._PROJECT_SETTINGS = view.window().project_data().get('settings', {})
        return self._PROJECT_SETTINGS

    def _get_project_variables(self, view):
        if not getattr(self, '_PROJECT_VARS', None):
            self._PROJECT_VARS = view.window().extract_variables()
        return self._PROJECT_VARS

    def _get_project_folder(self, view):
        return self._get_project_variables(view).get('folder')

    def _generate_settings(self, view):
        format_settings = self._get_project_settings(view).get('tommy_formats', {})
        project_path = self._get_project_folder(view)
        for k, v in format_settings.items():
            if '${project_path}' in v:
                yield (k, re.sub(r'\$\{project_path\}', project_path, v))

    def _get_settings(self, view):
        if not getattr(self, '_FORMAT_SETTINGS', None):
            self._FORMAT_SETTINGS = {k: v for k, v in self._generate_settings(view)}
        return self._FORMAT_SETTINGS

    def is_enabled(self, view):
        return ('tommy_formats' in self._get_project_settings(view))

    def get_project_interpreter_path(self, view):
        return '{}/bin/python'.format(self._get_settings(view).get('venv'))

    # def get_project_name(self, view):
    #     return self._get_project_variables(view).get('project_base_name')

    def get_project_activate_path(self, view):
        return os.path.join(
            self._get_settings(view).get('venv'),
            'bin', 'activate')

    def get_invoke_folder(self, view):
        return self._get_settings(view).get('invoke_folder')

    def get_file_extension(self, file_name):
        print("file_name: %s" % file_name)
        try:
            name, extension = os.path.splitext(file_name)
        except Exception:
            return
        extension = re.sub(r'\.', '', extension).lower()
        return extension

    def run_format(self, view, file_name=None):
        file_name = view.file_name()
        extension = self.get_file_extension(file_name)
        if extension not in ['py', 'js', 'vue']:
            return
        view.show_popup('Formating...')
        activate_path = self.get_project_activate_path(view)
        invoke_folder = self.get_invoke_folder(view)
        if extension in ['js', 'vue']:
            invoke_command = 'format-js'
        else:
            invoke_command = 'format'
        # TODO: debounce: given the time it takes to format a vue file this function should be debounced
        # TODO: prettier & eslint modify the file,
        # could the file be copied to memory then modified by both prior to writing to disk?
        argument = '. %(activate)s && cd %(invoke_dir)s && invoke %(invoke_command)s -f %(file_name)s' % {
            'activate': activate_path,
            'invoke_dir': invoke_folder,
            'invoke_command': invoke_command,
            'file_name': file_name,
        }
        process = subprocess.Popen(argument, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        return process.communicate(timeout=10)

    # Project Info: view.window().extract_variables()
    # Settings: view.window().project_data()

    def on_post_save_async(self, view):
        # if new task is triggered, kill all previous tasks, queue new task
        # how does threading work with sublime text? Would in-progress tasks be on a different thread?
        # Where are they managed?
        if not self.is_enabled(view):
            return
        if view.is_scratch():
            return

        try:
            outs, errs = self.run_format(view)
        except Exception:
            return
        resp = ""
        errors = ""
        if errs:
            errors = errs.decode('utf-8')
        if outs:
            resp = outs.decode('utf-8')
        if errors:
            print(errors)
        view.hide_popup()
        # if resp:
        #     print(resp)
