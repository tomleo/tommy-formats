# TommyFormats

Format your code on-save using [Invoke](https://docs.pyinvoke.org/en/1.3/)

## How it works

Create an *task* in your invoke script (`tasks.py`) called "format", the
signature should looke like this:

```python
@task(help={'filename': "Name of file to format"})
def format(c, filename=None):
    # ...
```

Use this function to call into your formatting script *du jour* (in practice I
call `isort` & `black`).

Example

```python
@task(help={'filename': "Name of file to format"})
def format(c, filename=None):
    """
    $ isort mypythonfile.py mypythonfile2.py ...
    $ black mypythonfile.py mypythonfile2.py ...
    """
    if filename:
        c.run(f"isort {filename}")
        c.run(f"black {filename}")
        return
    py_project_files_srt = " ".join(get_project_files())
    c.run(f"isort {py_project_files_srt}")
    c.run(f"black {py_project_files_srt}")
```

## Installation

1. symlink this project to your sublime-text installed packages folder

```cmd
[~/.config/sublime-text-3/Packages] $ sudo ln -s /home/tom/hacking/tommy-formats/format-on-save/ ./format-on-save/ 
```

2. Add the following to the `settings` section of your
   `<project-name>.sublime-project` file:

```cson
"tommy_formats": {
        "venv": "${project_path}/venv",
        "invoke_folder": "${project_path}/backend",
}
```


